package com.skyfarmer.wginfoservice.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.skyfarmer.wginfoservice.models.Departure;

import java.io.IOException;

public class DepartureDeserializer extends StdDeserializer<Departure> {

    public DepartureDeserializer() {
        this(null);
    }

    public DepartureDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Departure deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        var codec = jsonParser.getCodec();
        JsonNode node = codec.readTree(jsonParser);

        //For some reason, the MVV-API appends three extra zeroes
        var departureTime = node.get("departureTime").asLong() / 1000;
        var destination = node.get("destination").asText();
        var label = node.get("label").asText();
        Departure.Transportation product;
        switch (node.get("product").asText()) {
            case "UBAHN":
                product = Departure.Transportation.UNDERGROUND;
                break;
            case "REGIONAL_BUS":
                product = Departure.Transportation.BUS_REGIONAL;
                break;
            default:
                throw new InvalidFormatException(jsonParser, String.format("Unknown product: %s", node.get("product").asText()), node.get("product").asText(), Departure.class);
        }


        return new Departure(departureTime, destination, product, label);
    }
}
