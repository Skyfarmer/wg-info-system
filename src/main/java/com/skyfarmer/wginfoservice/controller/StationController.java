package com.skyfarmer.wginfoservice.controller;

import com.skyfarmer.wginfoservice.models.Alert;
import com.skyfarmer.wginfoservice.models.Departure;
import com.skyfarmer.wginfoservice.services.StationService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@EnableAutoConfiguration
@RequestMapping("/mvg")
public class StationController {

    private final StationService departureService;

    public StationController(StationService departureService) {
        this.departureService = departureService;
    }

    @GetMapping("/departures")
    public Flux<Departure> get_departures(@RequestParam(value = "station") String station) {
        return departureService.get_departures(departureService.resolve(station));
    }

    @GetMapping("/alerts")
    public Flux<Alert> get_alerts(@RequestParam(value = "station") String station) {
        return departureService.get_alerts(departureService.resolve(station));
    }
}
