package com.skyfarmer.wginfoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WgInfoServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WgInfoServiceApplication.class, args);
    }

}
