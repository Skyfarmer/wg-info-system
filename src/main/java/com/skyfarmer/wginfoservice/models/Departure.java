package com.skyfarmer.wginfoservice.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.skyfarmer.wginfoservice.deserializer.DepartureDeserializer;

@JsonDeserialize(using = DepartureDeserializer.class)
public class Departure implements Comparable {


    private long departure;
    private String destination;
    private Transportation transportation;
    private String label;

    public Departure(long departure, String destination,
                     Transportation transportation, String label) {
        this.departure = departure;
        this.destination = destination;
        this.transportation = transportation;
        this.label = label;
    }

    public long getDeparture() {
        return departure;
    }

    public String getDestination() {
        return destination;
    }

    public Transportation getTransportation() {
        return transportation;
    }

    public String getLabel() {
        return label;
    }

    public enum Transportation {
        BUS_REGIONAL,
        UNDERGROUND
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Departure) {
            var other = (Departure) o;
            return Long.compare(this.departure, other.departure);
        }

        throw new ClassCastException();
    }
}
