package com.skyfarmer.wginfoservice.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skyfarmer.wginfoservice.models.Alert;
import com.skyfarmer.wginfoservice.models.Departure;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import reactor.core.publisher.Flux;

import javax.validation.constraints.NotNull;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MvvStationService implements StationService {

    private final WebClient webClient;
    private Map<String, Long> stationMapping;

    public MvvStationService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://www.mvg.de")
                .defaultHeader("x-mvg-authorization-key", "5af1beca494712ed38d313714d4caff6")
                .build();
        this.stationMapping = new HashMap<>();

        stationMapping.put("Froettmaning", 470L);
    }

    public Flux<Departure> get_departures(long stationId) {
/*
        return this.webClient.get().uri("/fahrinfo/api/departure/{stationId}?footway=0", stationId)
                .exchange()
                .flatMap(response -> response.bodyToMono(String.class))
                .flatMapMany(json -> {
                    var gson = new GsonBuilder().registerTypeAdapter(Departure.class, new DepartureDeserializer()).create();
                    var lines = gson.fromJson(gson.fromJson(json, JsonElement.class).getAsJsonObject().get("departures"), Departure[].class);
                    return Flux.fromArray(lines);
                })
                .sort();
*/
        return this.webClient.get().uri("/fahrinfo/api/departure/{stationId}?footway=0", stationId)
                .exchange()
                .flatMap(response -> response.bodyToMono(JsonNode.class))
                .flatMapMany(json -> {
                    var mapper = new ObjectMapper();
                    var departures = mapper.convertValue(json.get("departures"), Departure[].class);
                    return Flux.fromArray(departures);
                })
                .sort();
    }

    @Override
    public Flux<Alert> get_alerts(long station_id) {
        return this.webClient.get().uri("/Tickerrss/CreateRssClass")
                .exchange()
                .flatMap(response -> response.bodyToMono(String.class))
                .flatMapMany(xml -> {
                    var factory = DocumentBuilderFactory.newInstance();
                    factory.setIgnoringElementContentWhitespace(true);
                    DocumentBuilder builder;
                    Document document;

                    try {
                        builder = factory.newDocumentBuilder();
                        //Data is read from memory (No network or file operations)
                        //Thus, this function will never block
                        //noinspection BlockingMethodInNonBlockingContext
                        document = builder.parse(new InputSource(new StringReader(xml)));
                    } catch (ParserConfigurationException | SAXException | IOException e) {
                        return Flux.error(e);
                    }
                    var items = document.getElementsByTagName("item");
                    if (items == null) {
                        return Flux.error(new RuntimeException("XML-Document does not contain any valid messages"));
                    }

                    List<Alert> alerts = new ArrayList<>();
                    for (int i = 0; i < items.getLength(); i++) {
                        var item = items.item(i);
                        String title = null, description = null;
                        for (int j = 0; j < item.getChildNodes().getLength(); j++) {
                            var currentNode = item.getChildNodes().item(j);
                            if (currentNode.getNodeName().equals("title")) {
                                title = currentNode.getTextContent();
                            } else if (currentNode.getNodeName().equals("description")) {
                                description = currentNode.getTextContent();
                            }
                        }
                        if (title != null && description != null) {
                            alerts.add(new Alert(title, description));
                        }
                    }
                    return Flux.fromArray(alerts.toArray(new Alert[0]));
                });
    }

    @Override
    public long resolve(@NotNull String name) throws StationNotFoundException {
        var v = stationMapping.get(name);
        if (v == null) {
            throw new StationNotFoundException(name);
        } else {
            return v;
        }
    }
}
