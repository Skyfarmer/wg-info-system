package com.skyfarmer.wginfoservice.services;

import com.skyfarmer.wginfoservice.models.Alert;
import com.skyfarmer.wginfoservice.models.Departure;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import reactor.core.publisher.Flux;

public interface StationService {

    Flux<Departure> get_departures(long station_id);

    Flux<Alert> get_alerts(long station_id);

    long resolve(String name) throws StationNotFoundException;

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    class StationNotFoundException extends RuntimeException {

        StationNotFoundException(String station) {
            super(String.format("Station %s does not exist", station));
        }
    }
}
